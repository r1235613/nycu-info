---
title: 校園公車資訊
description: 說明如何搭校車與公車到高鐵站、火車站、博愛校區與陽明校區，含公車時刻表。
keywords: [交大, 校車, 時刻表, 巴士, 客院]
sidebar_position: 1
---
# 公車資訊
:::warning
下午六點為竹科下班時間，因為可怕的交通狀況所以下午六點附近發車的車次通常會大幅延遲且無法預估；根據經驗週五交通狀況會好一些。
:::
### 交大光復校區 往返 新竹火車站
新竹市 2 路公車，全票15元   
乘車地點：光復校區大禮堂、新竹火車站食其家對面。   
火車站發車：0620, 0650, 0805, 0845, 1035, 1225, 1310, 1425, 1530, 1630, 1755, 1915, **1930**  
大禮堂發車：0645, 0725, 0840, 0920, 1110, 1300, 1345, 1500, 1600, 1705, 1830, 1945, **2010**  
1930, 2010於連假前一天行駛；0620, 0645於假期停駛，節假日時刻 [點我](https://ibus.hsinchu.gov.tw/ibusWeb/Businfo/rt) (公車種類：新竹市區公車、公車路線：【2】火車站—交大)。

### 交大光復校區 經 新莊車站 轉 高鐵站
去高鐵站還有另一條路，即從十三舍 7-11 後面的小路走出校門過馬路，搭上 81 (古奇峰 - 新莊車站) 的公車，到新莊車站轉台鐵到六家車站，然後再走天橋到高鐵站；不過這個班次比較雜：  
- [81 古奇峰](https://yunbus.tw/#!stop/HSZ303819)
- [新莊車站 \<-\> 六家車站](https://traintime.jsy.tw/search?s=1192&e=1194&d=2024-07-13&t=1100)

### 交大光復校區 往返 新竹高鐵 經 客家學院
乘車地點：光復校區大禮堂、竹北客家學院、高鐵新竹站。   
大禮堂發車：0705, 0830, 0945, 1145, 1300, **1400**, 1520, 1710, **1810, 2035, 2140**   
新竹站發車：0753, 0918, 1018, 1218, 1335, **1435**, 1615, 1740, **1850, 2120, 2215**   
例假日不行駛，粗體字為寒暑假停駛班次，校慶、畢典、補假時刻表同寒暑假，PDF[在此下載](https://ga.nycu.edu.tw/ga/ch/app/artwebsite/doc?module=artwebsite&detailNo=1233212857739382784&type=s)。
:::info
校車回校園會繞道女生第二宿舍、研三宿舍，可以不用在大禮堂先下車。
:::

### 交大光復校區 往返 交大博愛校區
乘車地點：光復校區大禮堂、清華正門、馬偕醫院、博愛街口、竹銘館、實驗動物中心。  
大禮堂發車：0710, **0815**, 0915, **1005**, 1215, **1315**, 1520, 1730, **1830, 2030, 2130, 2240**  
實驗動物中心：0725, **0840**, 0945, **1045**, 1250 ,**1400**, 1640, 1800, **1910, 2050, 2150, 2250**  
例假日不行駛，粗體字為寒暑假停駛班次，校慶、畢典、補假時刻表同寒暑假，PDF[在此下載](https://ga.nycu.edu.tw/ga/ch/app/artwebsite/doc?module=artwebsite&detailNo=1232943225237409792&type=s)。

### 交大光復校區 往返 交大陽明校區
乘車地點：交大光復行政大樓前、陽明校門口。  
上課日雙向對發：0630, 0800, 1215, 1430, 1630, 1830, 2040  
寒暑假雙向對發：0830, 1230, 1630  
例假日不行駛，校慶、畢典、補假時刻表同寒暑假，PDF[在此下載](https://ga.nycu.edu.tw/ga/ch/app/artwebsite/doc?module=artwebsite&detailNo=1232942384900214784&type=s****)。  
:::info
此校車之規則為持有搭車證(跨校區修課)優先、現教職員工生次之、最後為校友員眷。
:::
:::tip
週五下午晚上通常搭不到QQ
:::