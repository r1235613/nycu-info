import {themes as prismThemes} from 'prism-react-renderer';
import type {Config} from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

const config: Config = {
  title: '國立陽明交通大學交通資訊',
  tagline: '主要只有光復校區的交通資訊',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://nycu.info',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'r1235613', // Usually your GitHub org/user name.
  projectName: 'nycu-info', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'zh-Hant',
    locales: ['zh-Hant'],
  },

  presets: [
    [
      'classic',
      {
        docs: {
          sidebarPath: './sidebars.ts',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/r1235613/nycu-info/-/tree/master',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/r1235613/nycu-info/-/tree/master',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      } satisfies Preset.Options,
    ],
  ],

  themeConfig: {
    // Replace with your project's social card
    image: 'img/docusaurus-social-card.jpg',
    navbar: {
      title: '交通大學資訊站',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          type: 'docSidebar',
          sidebarId: 'tutorialSidebar',
          position: 'left',
          label: '文件',
        },
        {to: '/blog', label: '站長的碎碎念', position: 'left', activeBasePath: 'blog'},
        {
          href: 'https://gitlab.com/r1235613/nycu-info',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Links',
          items: [
            {
              label: '站長碎碎念',
              to: '/blog',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: '站長的FB',
              href: 'https://www.facebook.com/r1235613',
            },
            {
              label: '技術提供: docusaurus',
              href: 'https://docusaurus.io/',
            },
            //{
            //  label: 'Twitter',
            //  href: 'https://twitter.com/docusaurus',
            //},
          ],
        },
        {
          title: 'More',
          items: [
            //{
            //  label: 'Blog',
            //  to: '/blog',
            //},
            {
              label: 'GitLab Repo',
              href: 'https://gitlab.com/r1235613/nycu-info',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Yan-Ru Huang, Personal. Built with Docusaurus.`,
    },
    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
    },
  } satisfies Preset.ThemeConfig,
  scripts: [
    {
      src: 'https://static.cloudflareinsights.com/beacon.min.js',
      defer: true,
      "data-cf-beacon": '{"token": "1bdd7e241d524f00a3da5efdf4d2fbdb"}'
    },
  ],
};

export default config;
