import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';
import Link from '@docusaurus/Link';

type FeatureItem = {
  title: string;
  Svg: React.ComponentType<React.ComponentProps<'svg'>>;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: '交通資訊',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        蒐集了公車時刻表，包含到高鐵、到博愛校區、到火車站、到陽明山校區的巴士<br/>
        <Link
          className="button button--secondary button--lg"
          to="/docs/bus">
          交通時刻表
        </Link>
      </>
    ),
  },
  {
    title: '校園設施',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        打氣桶、廁所、印表機等等設施(待探索)
      </>
    ),
  },
  {
    title: '收件地址',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        不知道要如何收包裹嗎，來這裡就對了<br></br>
        <Link
          className="button button--secondary button--lg"
          to="/docs/mail">
          地址清單
        </Link>
      </>
    ),
  },
];

function Feature({title, Svg, description}: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
